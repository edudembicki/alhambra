(function($){
var form=$("#form_step1");
form.validate({ errorPlacement:function errorPlacement(error,element){element.before(error); },
	rules:{
		usuario_nome:{required:true,},
		usuario_nascimento:{required:true,}
	},
	messages:{email:{email:'Not a valid email address <i class="zmdi zmdi-info"></i>'}},
	onfocusout:function(element){$(element).valid();},
});

form.steps({
	headerTag:"h3",
	bodyTag:"fieldset",
	transitionEffect:"slideLeft",
	labels:{
		previous:'Anterior',
		next:'Próximo',
		finish:'Concluir',
		current:''},
	titleTemplate:'<div class="title"><span class="number">#index#</span>#title#</div>',

	onStepChanging:function(event,currentIndex,newIndex){
		if(currentIndex == 0){
			$.ajax({
			    
			    url : window.location.href+'index.php/home/step1',
			    type : 'GET',
			    data : {
			        'nome' : usuario_nome.value,
				'nascimento' : usuario_nascimento.value
			    },
			    dataType:'json',
			    success : function(data) {
				    $("#usuario_id").val(data);
			    },
			    error : function(request,error)
			    {
				    $("#usuario_id").val(data);
			    }
			});
		}
		if(currentIndex == 1){
			$.ajax({
			    
			    url : window.location.href+'index.php/home/step2',
			    type : 'GET',
			    data : {
			        'endereco_usuario_id'	: usuario_id.value,
			        'logradouro' 		: endereco_logradouro.value,
				'numero' 		: endereco_numero.value,
			        'cep' 			: endereco_cep.value,
			        'cidade'	 	: endereco_cidade.value,
			        'estado' 		: endereco_estado.value,
			    },
			    dataType:'json',
			    success : function(data) {              
			    },
			    error : function(request,error)
			    {
			    }
			});
		}
		form.validate().settings.ignore=":disabled,:hidden";
		return form.valid();
	},
	
	onFinishing:function(event,currentIndex){
		$.ajax({
			    
		     url : window.location.href+'index.php/home/step3',
		     type : 'GET',
		     data : {
		         'contato_usuario_id'	: usuario_id.value,
		         'contato_tel_fixo'	: contato_tel_fixo.value,
		         'contato_tel_celular'	: contato_tel_celular.value,
		     },
		     dataType:'json',
		     success : function(data) {              
		     },
		     error : function(request,error)
		     {
		     }
		});

		alert('Cadastro realizado com sucesso!');
		location.reload();
		form.validate().settings.ignore=":disabled";console.log(getCurrentIndex);return form.valid();
	},
		
	onFinished:function(event,currentIndex){
		
	},

});
	
jQuery.extend(jQuery.validator.messages,{required:"",remote:"",url:"",date:"",dateISO:"",number:"",digits:"",creditcard:"",equalTo:""});$.dobPicker({daySelector:'#expiry_date',monthSelector:'#expiry_month',yearSelector:'#expiry_year',dayDefault:'DD',yearDefault:'YYYY',minimumAge:0,maximumAge:120});$('#password').pwstrength();$('#button').click(function(){$("input[type='file']").trigger('click');})
$("input[type='file']").change(function(){$('#val').text(this.value.replace(/C:\\fakepath\\/i,''))})})(jQuery);
