<?php
class Savesteps_model extends CI_Model {
      
    function step1()
    {
	$data = array(
	        'usuario_id' 		=> 0,
	        'usuario_nome' 		=> $_REQUEST['nome'],
        	'usuario_nascimento' 	=> $_REQUEST['nascimento'],
	        'usuario_status' 	=> 1,
	        'usuario_dt_cadastro'	=> date('Y-m-d H:i:s')
	);

	$this->db->insert('tb_usuarios', $data);

	print_r($this->db->insert_id());
    }
    
    function step2()
    {
	$data = array(
		'endereco_id'		=> 0,
	        'endereco_usuario_id'	=> $_REQUEST['endereco_usuario_id'],
	        'endereco_logradouro'	=> $_REQUEST['logradouro'],
	        'endereco_numero'	=> $_REQUEST['numero'],
        	'endereco_cep'	 	=> $_REQUEST['cep'],
	        'endereco_cidade' 	=> $_REQUEST['cidade'],
	        'endereco_uf'		=> $_REQUEST['estado'],
	        'endereco_status' 	=> 1,
	        'endereco_dt_cadastro'	=> date('Y-m-d H:i:s')
	);

	$this->db->insert('tb_enderecos', $data);
    }
    
    function step3()
    {
	$data = array(
		'contato_id'		=> 0,
	        'contato_usuario_id'	=> $_REQUEST['contato_usuario_id'],
	        'contato_tel_fixo'	=> $_REQUEST['contato_tel_fixo'],
	        'contato_tel_celular'	=> $_REQUEST['contato_tel_celular'],
	        'contato_status' 	=> 1,
	        'contato_dt_cadastro'	=> date('Y-m-d H:i:s')
	);

	$this->db->insert('tb_contatos', $data);
    }

}
?>
