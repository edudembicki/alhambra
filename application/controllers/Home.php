<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
        	parent::__construct();
		$this->load->helper('url');
	}
	
	public function index()
	{
		//Carrega Models de estados e cidades
		$this->load->model('Cidades_model');
		$this->load->model('Estados_model');

		//Carrega os dados de estados e cidades
		$dados['cidades'] = $this->Cidades_model->dados();
		$dados['estados'] = $this->Estados_model->dados();

		//Carrega a view, passando estados e cidades
		$this->load->view('index', $dados);
	}
	
	public function step1()
	{
		//Insere os dados na tabela tb_usuarios atraves de model
		$this->load->model('Savesteps_model');
		$this->Savesteps_model->step1();
	}
	
	public function step2()
	{
		//Insere os dados na tabela tb_usuarios atraves de model
		$this->load->model('Savesteps_model');
		$this->Savesteps_model->step2();
	}
	
	public function step3()
	{
		//Insere os dados na tabela tb_usuarios atraves de model
		$this->load->model('Savesteps_model');
		$this->Savesteps_model->step3();
	}	
}
