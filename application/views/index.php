<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="colorlib.com">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Formulario Alhambra</title>

    <link rel="stylesheet" href="<?=base_url('assets/css/material-design-iconic-font.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/style.css')?>">
</head>

<body>
    <div class="main" style="background: url('<?=base_url('assets/img/banner_topo_bg.png')?>'); background-size: cover;">
        <div class="container">
            <h2>Formulário de Cadastro </h2>
            <form method="POST" id="form_step1" class="signup-form">
                <h3>
		    <span class="title_text">Informações Pessoais</span>
		</h3>
                <fieldset>
                    <div class="fieldset-content">
                        <div class="form-group">
			    <label for="usuario_nome" class="form-label">Nome Completo</label>
                            <input type="text" name="usuario_nome" id="usuario_nome" placeholder="Nome Completo" />
                        </div>
                        <div class="form-group">
                            <label for="usuario_nascimento" class="form-label">Data Nascimento</label>
                            <input type="date" name="usuario_nascimento" id="usuario_nascimento" placeholder="Data Nascimento" />
                        </div>
                    </div>
                    <div class="fieldset-footer">
                        <span>Passo 1 de 3</span>
                    </div>
		</fieldset>
                <h3>
		    <span class="title_text">Endereço</span>
		</h3>
                <fieldset>
		    <div class="fieldset-content">
			<input type="hidden" name="usuario_id" id="usuario_id" value="" />
			<div class="form-group" style="width: 50%; float: left;">
                            <label for="endereco_logradouro" class="form-label">Logradouro</label>
                            <input type="text" name="endereco_logradouro" id="endereco_logradouro" placeholder="Rua / Travessa / Avenida" />
                        </div>
			<div class="form-group" style="width: 50%; float: left;">
                            <label for="endereco_numero" class="form-label">Número</label>
                            <input type="number" name="endereco_numero" id="endereco_numero" placeholder="Número" />
                        </div>
			<div class="form-group" style="width: 50%; float: left;">
                            <label for="endereco_cep" class="form-label">CEP</label>
                            <input type="text" class="cep" name="endereco_cep" id="endereco_cep" placeholder="CEP" />
			</div>
			<div class="form-group" style="width: 50%; float: left;">
                            <label for="endereco_cidade" class="form-label">Cidade</label>
			    <select name="endereco_cidade" id="endereco_cidade">
			    	<?php foreach($cidades as $valor_cidades){ ?>
					<option value="<?=$valor_cidades['id']?>"><?=$valor_cidades['nome']?></option>
				<?php } ?>
			    </select>
                        </div>
			<div class="form-group" style="width: 50%; float: left;">
			    <label for="endereco_estado" class="form-label">Estado</label>
			    <select name="endereco_estado" id="endereco_estado">
			    	<?php foreach($estados as $valor_estados){ ?>
					<option value="<?=$valor_estados['id']?>"><?=$valor_estados['nome']?></option>
			    	<?php } ?>
			    </select>
                        </div>

                    </div>
                    <div class="fieldset-footer">
                        <span>Passo 2 de 3</span>
                    </div>
                </fieldset>
                <h3>
		    <span class="title_text">Contato</span>
		</h3>
                <fieldset>
                    <div class="fieldset-content">
                        <div class="form-group">
                            <label for="contato_tel_fixo" class="form-label">Telefone Fixo</label>
                            <input type="text" class="telefone" name="contato_tel_fixo" id="contato_tel_fixo" />
			</div>
			<div class="form-group">
                            <label for="contato_tel_celular" class="form-label">Telefone Celular</label>
                            <input type="text" class="telefone" name="contato_tel_celular" id="contato_tel_celular" />
                        </div>
                    </div>
                    <div class="fieldset-footer">
                        <span>Passo 3 de 3</span>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

    <script src="<?=base_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?=base_url('assets/js/jquery.validate.min.js')?>"></script>
    <script src="<?=base_url('assets/js/additional-methods.min.js')?>"></script>
    <script src="<?=base_url('assets/js/jquery.steps.min.js')?>"></script>
    <script src="<?=base_url('assets/js/dobpicker.js')?>"></script>
    <script src="<?=base_url('assets/js/jquery.pwstrength.js')?>"></script>
    <script src="<?=base_url('assets/js/main.js')?>"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
    <script>
	jQuery("input.telefone")
        .mask("(99) 9999-9999?9")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
	});
	jQuery("input.cep")
        .mask("99999-999")
        .focusout(function (event) {  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("99999-999");  
            } else {  
                element.mask("99999-999");  
            }  
        });

    </script>
</body>

</html>
